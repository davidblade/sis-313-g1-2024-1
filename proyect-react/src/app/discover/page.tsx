import Service from "@/componets/Services"
import Title from "@/componets/Title"

const Discover = () => {
    return (
        <section className="container services">
        <div>
            <Title subtitle="DISCOVER" title="Our top value categories for you"/>
        </div>
        <div>
            <div className="content-services shadow-scroll-x">
                <Service image="/images/service1.svg" title="Best Tour Guide" paragrah="What looked like a small patch of purple grass, above five feet."/>
                <Service image="/images/service2.svg" title="Easy Booking" paragrah="Square, was moving across the sand in their direction."/>
                <Service image="/images/service1.svg" title="Best Tour Guide" paragrah="What looked like a small patch of purple grass, above five feet."/>
                <Service image="/images/service2.svg" title="Easy Booking" paragrah="Square, was moving across the sand in their direction."/>
                <Service image="/images/service1.svg" title="Best Tour Guide" paragrah="What looked like a small patch of purple grass, above five feet."/>
            </div>
        </div>
    </section>
    )
}
export default Discover