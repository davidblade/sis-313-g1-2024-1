import Badge from "@/componets/Badge"
import { Feacture } from "@/componets/Feacture"
import Title from "@/componets/Title"

const Contact = () => {
    return (
        <section className="container features">
            <div>
                <Title subtitle="CONTACT" title="We offer best services" paragraph="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC."/>
                <div className="content-features">
                    <Feacture icon="icon-pointer" title="We offer best services" paragraph="Lorem Ipsum is not simply random text"/>
                    <Feacture icon="icon-calendar" title="We offer best services" paragraph="Lorem Ipsum is not simply random text" bgcolor="#facd49"/>
                    <Feacture icon="icon-ticket" title="We offer best services" paragraph="Lorem Ipsum is not simply random text" bgcolor="#f85e9f"/>
                </div>
            </div>
            <div>
                <div className="bg-1-feacture"></div>
                <div className="bg-2-feacture"></div>
                <div className="bg-3-feacture"></div>
                <span className="badge-feacture">
                    <Badge text="Explore the world!" icon="icon-map" />
                </span>
            </div>
        </section>
    )
}

export default Contact