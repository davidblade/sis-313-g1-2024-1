'use client';
import Button from "@/componets/Button"
import Destine from "@/componets/Destine"
import Title from "@/componets/Title"
import { eventButtonLeft, eventButtonRight } from "@/helpers/helpers";
import { useRef } from "react";

const SpecialDeals = () => {
    const myRef:any = useRef(null);
    return (
        <section className="container destine">
            <div>
                <Title subtitle="Top Destination" title="Explore top destination"/>
                <div className="scroll-btn-destine">
                    <Button transparent border circle aloneIcon aloneIconName="arrow-left" onClick={()=>{eventButtonLeft(myRef)}}></Button>
                    <Button primary circle aloneIcon aloneIconName="arrow-right" onClick={()=>{eventButtonRight(myRef)}}></Button>
                </div>
            </div>
            <div id="list-destines" ref={myRef}>
                <div className="content-destine">
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                </div>
            </div>
        </section>
    )
}

export default SpecialDeals