'use client';

import Image from "next/image";
import Button from "@/componets/Button";
import Logo from "@/componets/Logo";
import Badge from "@/componets/Badge";
import Title from "@/componets/Title";
import Service from "@/componets/Services";
import Destine from "@/componets/Destine";
import { Company } from "@/componets/Company";
import { Feacture } from "@/componets/Feacture";
import { useRef } from 'react';
import { eventButtonLeft, eventButtonRight } from "@/helpers/helpers";

export default function Home() {
  const myRef:any = useRef(null);
  return (
    <main>
        <div className="bg1-hero"></div>
        <section className="container hero">
            <div>
                <Badge text="Explore the world!" icon="icon-one" reverse/>
                <div className="title-hero">
                    <h1>Travel <span className="text-primary-one">top destination</span> of the world</h1>
                    <p>We always make our customer happy by providing as many choices as possible </p>
                    <div>
                        <Button title="Get Started" primary shadown></Button>
                        <Button title="Whatch Demo" transparent border icon></Button>
                    </div>
                </div>
            </div>
            <div>
                <div className="design-images-hero">
                    <div className="hero-one">
                        <Image src="/images/hero1.png" alt="" width={100} height={100}/>
                        <Image src="/images/hero2.png" alt="" width={100} height={100}/>
                    </div>
                    <div className="hero-two">
                      <Image src="/images/hero3.png" alt="" width={100} height={100}/>
                    </div>
                </div>
                <div className="bg0-hero"></div>
            </div>
        </section>
        <div className="sub-hero">
            <div className="bg2-hero"></div>
            <div className="container icons-companys">
                <Company image="/images/company/company1.svg"/>
                <Company image="/images/company/company2.svg"/>
                <Company image="/images/company/company3.svg"/>
                <Company image="/images/company/company4.svg"/>
                <Company image="/images/company/company5.svg"/>
            </div>
            <div className="bg3-hero">adsadas</div>
        </div>
        <section className="container services">
            <div>
                <Title subtitle="services" title="Our top value categories for you"/>
            </div>
            <div>
                <div className="content-services shadow-scroll-x">
                    <Service image="/images/service1.svg" title="Best Tour Guide" paragrah="What looked like a small patch of purple grass, above five feet."/>
                    <Service image="/images/service2.svg" title="Easy Booking" paragrah="Square, was moving across the sand in their direction."/>
                    <Service image="/images/service1.svg" title="Best Tour Guide" paragrah="What looked like a small patch of purple grass, above five feet."/>
                    <Service image="/images/service2.svg" title="Easy Booking" paragrah="Square, was moving across the sand in their direction."/>
                    <Service image="/images/service1.svg" title="Best Tour Guide" paragrah="What looked like a small patch of purple grass, above five feet."/>
                </div>
            </div>
        </section>
        
        <section className="container destine">
            <div>
                <Title subtitle="Top Destination" title="Explore top destination"/>
                <div className="scroll-btn-destine">
                    <Button transparent border circle aloneIcon aloneIconName="arrow-left" onClick={()=>{eventButtonLeft(myRef)}}></Button>
                    <Button primary circle aloneIcon aloneIconName="arrow-right" onClick={()=>{eventButtonRight(myRef)}}></Button>
                </div>
            </div>
            <div id="list-destines" ref={myRef}>
                <div className="content-destine">
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                    <Destine title="Paradise Beach, Bantayan Island" image="/images/destine1.png" price={550.16} paragrah="Rome, Italy" starts={4.8}></Destine>
                </div>
            </div>
        </section>

        <section className="container features">
            <div>
                <Title subtitle="Key features" title="We offer best services" paragraph="Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC."/>
                <div className="content-features">
                    <Feacture icon="icon-pointer" title="We offer best services" paragraph="Lorem Ipsum is not simply random text"/>
                    <Feacture icon="icon-calendar" title="We offer best services" paragraph="Lorem Ipsum is not simply random text" bgcolor="#facd49"/>
                    <Feacture icon="icon-ticket" title="We offer best services" paragraph="Lorem Ipsum is not simply random text" bgcolor="#f85e9f"/>
                </div>
            </div>
            <div>
                <div className="bg-1-feacture"></div>
                <div className="bg-2-feacture"></div>
                <div className="bg-3-feacture"></div>
                <span className="badge-feacture">
                    <Badge text="Explore the world!" icon="icon-map" />
                </span>
            </div>
        </section>
    </main>
  );
}
