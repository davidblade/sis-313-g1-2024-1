interface FeactureProps {
    icon: string,
    title: string,
    paragraph: string,
    bgcolor?: string
}

export const Feacture = (props:FeactureProps) => {
    return (
        <div className="item-feacture">
            <span style={{backgroundColor:props.bgcolor??"#ff5722"}}>
                <i className={`icon ${props.icon}`}></i>
            </span>
            <div>
                <h4>{props.title}</h4>
                <p>{props.paragraph}</p>
            </div>
        </div>
    )
}