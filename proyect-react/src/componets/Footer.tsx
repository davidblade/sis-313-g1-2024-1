import FeatherIcon from "feather-icons-react"
import Logo from "./Logo"

interface itemFooterProps {
    title: string;
    items: string[];
}
const ItemFooter = (props:itemFooterProps) => {
    return (
        <div>
            <h6>{props.title}</h6>
            <ul>
                {props.items.map((item:string,index:number)=>{
                    return <li key={index}>
                        <a href="#">{item}</a>
                    </li>
                })}
            </ul>
        </div>
    )
}

export const Footer =()=>{
    return (
        <footer className="container">
            <div>
                <Logo/>
                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC.</p>
                <div className="icons-footer">
                    <span><FeatherIcon size={23} icon="facebook" fill="#ffffff" strokeWidth={0.2}/></span>
                    <span><FeatherIcon size={23} icon="twitter" fill="#ffffff" strokeWidth={0.2}/></span>
                    <span><FeatherIcon size={23} icon="instagram"/></span>
                </div>
            </div>
            <div>
                <ItemFooter title="Company" items={["About","Career","Mobile"]}/>
                <ItemFooter title="Contact" items={["Why Travlog?","Partner with us","FAQ’s","Blog"]}/>
                <ItemFooter title="Meet Us" items={["+00 92 1234 56789","info@travlog.com","205. R Street, New York","BD23200"]}/>
            </div>
        </footer>
    )
}