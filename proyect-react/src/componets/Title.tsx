interface TitleProps{
    title:string,
    subtitle:string,
    sizeTextTitle?:number,
    paragraph?:string
}

const Title=({title,subtitle,sizeTextTitle,paragraph}:TitleProps)=>{
    return(
        <div className="title">
            <h4 style={{textTransform:"uppercase"}}>{subtitle}</h4>
            <h3 style={{fontSize:sizeTextTitle?`${sizeTextTitle}px`:"30px"}}>{title}</h3>
            {paragraph?<p>{paragraph}</p>:""}
        </div>
    )
}

export default Title