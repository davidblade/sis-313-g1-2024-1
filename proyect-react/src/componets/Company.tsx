import Image from "next/image";

interface CompanyProps {
    image: string;
}

export const Company = (props:CompanyProps) => {
    return (
        <div className="icons-companys">
            <Image src={props.image} alt="" width={100} height={100}/>
        </div>
    )
}