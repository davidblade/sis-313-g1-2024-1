type BadgeProps = {
    text:string,
    icon:string,
    reverse?:boolean,
    color?:string
}

const Badge = (props:BadgeProps) =>{
    const {text, icon, reverse} = props;
    const defaultClass = `icon ${icon}`
    return(
        <div className={reverse?`badge-reverse badge ${props.color??'#f85e9f'}`:`badge ${props.color??'#f85e9f'}`}>
            <i className={defaultClass}></i>
            <span>{text}</span>
        </div>
    )
}

export default Badge;
