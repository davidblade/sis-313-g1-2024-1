import Image from "next/image"

type ServiceProps = {
    image:string,
    title:string,
    paragrah:string
}
const Service =({image,title,paragrah}:ServiceProps)=>{
    return (
        <div className="item-service">
            <Image src={image} alt="" width={100} height={100}/>
            <div>
                 <h3>{title}</h3>
                 <p>{paragrah}</p>
            </div>
        </div>
    )
}
export default Service