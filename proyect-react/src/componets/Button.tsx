'use client';

import FeatherIcon from 'feather-icons-react';

//type PropsButton = {
interface PropsButton{
    title?:string,
    primary?:boolean,
    transparent?:boolean,
    shadown?:boolean,
    border?:boolean,
    icon?:boolean,
    circle?:boolean,
    aloneIcon?:boolean,
    aloneIconName?:string
    onClick?:()=>void
}


export default function Button(props:PropsButton){
    let classDefault:any = "btn"
    const designButton:any = {
        primary:"btn-primary",
        transparent:"btn-transparent",
        shadown:"btn-shadown",
        border:"btn-border",
        circle:"btn-circle"
    }
    
    const setProps:any = JSON.parse(JSON.stringify(props))
    for (const key in setProps) {//ITERAMOS EN TODAS LAS PROPIEDAS QUE TIENE PROPS
        if (props.hasOwnProperty(key) && designButton[key]) {// VERIFICO SI LA PROPIEDAD EXISTE EN PROPS
                classDefault += ` ${designButton[key]}`
        }
    }
    
    return (
        props.aloneIcon?
            <button className={classDefault} onClick={props.onClick}>
                <FeatherIcon icon={props.aloneIconName??''} />
            </button>
        :
            <button className={classDefault}>
                {props.icon?<i className="icon icon-play"></i>:''}
                <span>{props.title}</span>
            </button>
    )
}