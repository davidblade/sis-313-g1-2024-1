import Button from './Button'
import Logo from './Logo'
import Link from 'next/link'

const styleNav={
    // backgroundColor:"blue"
}
export function Header(){
    return (
        <div className="container nav-header" style={styleNav}>
            <Logo/>
            <ul>
                <li>
                    <Link href="/" className='active'>Home</Link>
                </li>
                <li>
                    <Link href="/discover">Discover</Link>
                </li>
                <li>
                    <Link href="/special-deals">Special Deals</Link>
                </li>
                <li>
                    <Link href="/contact">Contact</Link>
                </li>
            </ul>
            <div>
                <Button title="Log In" transparent={true}/>
                <Button title="Sign Up" primary/>
            </div>
        </div>
    )
}