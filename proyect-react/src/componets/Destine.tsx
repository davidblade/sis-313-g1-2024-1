import FeatherIcon from "feather-icons-react";
import Image from "next/image";

interface DestineProps {
    image: string;
    title: string;
    price: number
    paragrah: string;
    starts: number;
}
const Destine = ({image,title,price,paragrah,starts}:DestineProps) => {
    return (
        <div className="destine-item">
            <div>
                <Image src={image} alt="" width={100} height={100}/>
            </div>
            <div className="detail-destine">
                <div>
                    <h6>{title}</h6>
                    <span>${price}</span>
                </div>
                <p>{paragrah}</p>
                <span>{starts} <FeatherIcon size={23} icon="star" fill="#ff5722" /></span> 
            </div>
        </div>
    );
}

export default Destine;