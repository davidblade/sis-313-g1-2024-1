"use client"
import * as React from 'react';
import Box from '@mui/material/Box';
import MyCard from '@/components/myCard';
import { Button, Grid, TextField, ThemeProvider, Typography } from '@mui/material';
import Image from 'next/image';
import { themePersonal } from '@/helpers/theme';
import { CardLanguages } from '@/components/CardLanguages';
import CardService from '@/components/CardServices';
import { getPokemon } from '@/services/pokemon.service';
import { usePokemon } from '@/hooks/usePokemon';
import { useRickAndMorty } from '@/hooks/useRickAndMorty';
import { Loading } from '@/components/Loading';
import {ChevronRight} from '@mui/icons-material';

const centerFlex = { display: "flex", justifyContent: "center", alignItems: "center" }
const flexDirectionColumn = { flexDirection: "column" }



export default function Home() {
  const { pokemon, loading: loadingPokemon } = usePokemon()
  const { characters, loading: loadingRickAndMorty } = useRickAndMorty()
  const checkLoading: boolean = loadingPokemon && loadingRickAndMorty
  console.log(checkLoading)
  // console.log(loadingPokemon?'CARGANDO...':'')
  // if(!loadingPokemon){
  //   console.log(pokemon)
  // }
  console.log(loadingRickAndMorty ? 'CARGANDO...' : '')
  if (!loadingRickAndMorty) {
    console.log(characters)
  }
  if (checkLoading)
    return <Loading loading={checkLoading}></Loading>
  else
    return (
      <ThemeProvider theme={themePersonal}>
        <Loading loading={checkLoading} />
        <Box sx={{ display: 'flex' }}>
          <Box component="main" sx={{ p: 3, width: "100%" }}>
            {/* <MyCard /> */}
            <Grid container spacing={2}>
              <Grid item xs={6} sx={{ ...centerFlex, ...flexDirectionColumn }}>
                <Typography variant="h1" gutterBottom sx={{fontWeight:"bold"}}>
                  Hi, i’m Dasteen Front end dev
                </Typography>
                <Box>
                  <Typography variant="h6" gutterBottom className='paragraph border-left'>
                    On this blog I share tips and tricks, frameworks, projects, tutorials, etc Make sure you subscribe to get the latest updates
                  </Typography>
                </Box>
                <Box sx={{ ...centerFlex, justifyContent: 'space-between', width: '100%', marginTop:"20px" }}>
                  <TextField fullWidth id="outlined-basic" label="Enter your email here...." variant="outlined" sx={{marginRight:"20px"}} />
                  <Button variant="contained" sx={{height:"53px",marginlLeft:"10px"}}>Subscribe</Button>
                </Box>
              </Grid>
              <Grid item xs={6} sx={centerFlex}>
                <Image src="/images/heroImg.svg" alt="Vercel Logo" width={500} height={500} />
              </Grid>
              <Grid item xs={12}>
                <Box sx={{height:"70px",display: "flex",alignItems:"center", justifyContent: "space-between", marginBottom: "20px"}}>
                  <Typography variant="h5" gutterBottom className='pagination-item'>
                    Browse the category
                  </Typography>
                  <Button variant="text" className='pagination-item'>See all Categories <ChevronRight sx={{fontSize:"30px"}} /></Button>
                </Box>
                <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                  <CardLanguages title='CSS3' image='/images/icon1.svg'></CardLanguages>
                  <CardLanguages title='CSS3' image='/images/icon1.svg'></CardLanguages>
                  <CardLanguages title='CSS3' image='/images/icon1.svg'></CardLanguages>
                  <CardLanguages title='CSS3' image='/images/icon1.svg'></CardLanguages>
                  <CardLanguages title='CSS3' image='/images/icon1.svg'></CardLanguages>
                </Box>
              </Grid>
            </Grid>


            <Grid container spacing={2} sx={{marginY:"40px"}}>
              <Grid item xs={12}>
              <Box sx={{height:"70px",display: "flex",alignItems:"center", justifyContent: "space-between", marginBottom: "20px"}}>
                  <Typography variant="h5" gutterBottom className='pagination-item'>
                    Browse the category
                  </Typography>
                  <Button variant="text" className='pagination-item'>See all Categories <ChevronRight sx={{fontSize:"30px"}} /></Button>
                </Box>
              </Grid>
              {pokemon?.results.map((item: any, index: number) => {
                const splitUrl = item.url.split('/')
                const id = splitUrl[splitUrl.length - 2]
                return (
                  <Grid key={index} item xs={12} sm={12} md={6} lg={3} xl={3}>
                    <CardService
                      avatar='A'
                      title={item.name}
                      image={`${process.env.NEXT_PUBLIC_IMAGES_POKEMON}/${id}.png`}
                      name='David'
                      date='Jan 10, 2022 ∙ 3 min read'>
                    </CardService>
                  </Grid>
                )
              })}
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Box sx={{height:"70px",display: "flex",alignItems:"center", justifyContent: "space-between", marginBottom: "20px"}}>
                  <Typography variant="h5" gutterBottom className='pagination-item'>
                    Browse the category
                  </Typography>
                  <Button variant="text" className='pagination-item'>See all Categories <ChevronRight sx={{fontSize:"30px"}} /></Button>
                </Box>
              </Grid>
              {characters?.results.map((item: any, index: number) => {
                return (
                  <Grid key={index} item xs={12} sm={12} md={6} lg={3} xl={3}>
                    <CardService
                      avatar={item.name.split('')[0]}
                      title={item.name}
                      image={item.image}
                      name={item.species}
                      date={item.location.name}>
                    </CardService>
                  </Grid>
                )
              })}
            </Grid>
          </Box>
        </Box>
      </ThemeProvider>
    );
}

