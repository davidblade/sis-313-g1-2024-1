import axios from "axios";

export const instancerRickAndMorty = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_RICK_AND_MORTY,
    timeout: 5000,
    headers: { 'X-Custom-Header': 'foobar' }
});

instancerRickAndMorty.interceptors.request.use(function (config) {    
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});