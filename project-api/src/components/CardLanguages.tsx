import { Card, CardContent, CardMedia, Typography } from "@mui/material"
import React from "react"

interface propsCardLanguages {
    image:string,
    title:string,
}

export const CardLanguages = ({image,title}:propsCardLanguages) => {
    return (
        <React.Fragment>
            <Card className="card-languages">
                <CardMedia
                    component="img"
                    image={image}
                    className="card-media-img-languages"

                />
                <CardContent>
                    <Typography sx={{ fontSize: 30 }} color="text.secondary" gutterBottom>
                        {title}
                    </Typography>
                </CardContent>
            </Card>
        </React.Fragment>
    )
}