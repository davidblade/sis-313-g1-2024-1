import { Box, Grid, List, ListItem, ListItemText, Typography } from "@mui/material"
import { Logo } from "./Logo"

interface propsDetailLinks {
    name: string,
    url: string
}

interface propsLinksFooter {
    title: string,
    links: propsDetailLinks[]
}

const generateLinks = (links:propsDetailLinks[]) =>{
    return (
        links.map(e=>{
            return (
                <ListItem>
                    <ListItemText
                        primary={e.name}
                    />
                </ListItem>
            )
        })
    )
}

const LinksFooter = ({ title, links }: propsLinksFooter) => {
    return (
        <Box>
            <Typography variant="h5">{title}</Typography>
            <List>
                {generateLinks(links)}
            </List>
        </Box>
    )
}
export const Footer = () => {
    const elementsLinks:propsLinksFooter[] = [
        {
            title:"Category",
            links:[
                {name:"CSS",url:"#"},
                {name:"Javascript",url:"#"},
                {name:"Tailwind",url:"#"},
                {name:"React JS",url:"#"},
                {name:"More Category",url:"#"},
            ]
        },
        {
            title:"About Me",
            links:[
                {name:"About Me",url:"#"},
                {name:"Projects",url:"#"},
                {name:"Achievement",url:"#"},
            ]
        },
        {
            title:"Get in touch",
            links:[
                {name:"+62-8XXX-XXX-XX",url:"#"},
                {name:"demo@gmail.com",url:"#"},
            ]
        },
        {
            title:"Follow Us",
            links:[
                {name:"Medium",url:"#"},
                {name:"Instagram",url:"#"},
                {name:"Twitter",url:"#"},
                {name:"Facebook",url:"#"},
            ]
        }
    ]
    return (
        <Grid container spacing={2} className="footer">
            <Grid item xs={3}>
                <Logo />
                <Typography variant="body2">
                    Digitaldastin by Dastin Darmawan
                </Typography>
            </Grid>
            <Grid item xs={9}>
                <Grid container spacing={2}>
                    {elementsLinks.map(e=>{
                        return (
                            <Grid item xs={3}>
                                <LinksFooter title={e.title} links={e.links}/>
                            </Grid>
                        )
                    })}
                </Grid>
            </Grid>
        </Grid>
    )
}