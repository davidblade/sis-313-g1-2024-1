import { instancerRickAndMorty } from "@/configuration/config-rickAndMorty"

export const getCharacters = async ()=>{
    const response = await instancerRickAndMorty.get('/character')
    return response.data
}